# Doob Bot 
[![Discord Bots](https://top.gg/api/widget/status/624829444963696660.svg?noavatar=true)](https://top.gg/bot/624829444963696660)
[![Discord Bots](https://top.gg/api/widget/upvotes/624829444963696660.svg?noavatar=true)](https://top.gg/bot/624829444963696660)
[![Discord Bots](https://top.gg/api/widget/lib/624829444963696660.svg?noavatar=true)](https://top.gg/bot/624829444963696660)
[![Discord Bots](https://discordbots.org/api/widget/owner/624829444963696660.svg?noavatar=true)](https:/top.gg/bot/624829444963696660)
[![GitHub issues](https://img.shields.io/github/issues/mmatt625/thecave.svg)](https://github.com/mmatt625/thecave/issues)
[![GitHub forks](https://img.shields.io/github/forks/mmatt625/thecave.svg)](https://github.com/mmatt625/thecave/network)
[![GitHub stars](https://img.shields.io/github/stars/mmatt625/thecave.svg)](https://github.com/mmatt625/thecave/stargazers)
[![GitHub license](https://img.shields.io/github/license/mmatt625/thecave.svg)](https://github.com/mmatt625/thecave/blob/master/LICENSE)
[![The Cave Discord](https://discordapp.com/api/guilds/560262402659057681/widget.png?style=shield)](https://discord.gg/8xMWb7W)

# About
The Doob Bot is a new Discord bot coded by mmatt#0001.

## Can I use Doob right now?
No, but you can contribute to the code, since it's open sourced.

## Issues or want to improve some code?
Submit an [Issue](https://github.com/mmatt-studios/doob/issues) or a [Pull request](https://github.com/mmatt-studios/doob/pulls).
